# lesson1.4
**multistage**
# BUILD
```
docker build -t lesson1.4 .
```

# RUN CONTAINER
```
docker run -d -p 80:80 -p 443:443 --volume $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf  --volume $(pwd)/index.html:/usr/local/nginx/html/index.html lesson1.4
```

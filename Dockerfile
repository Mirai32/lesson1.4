FROM debian:9 as build
ENV NGINX_VERSION=1.17.0 \
    LUAJIT_VERSION=v2.1-20200102 \
    NGINX_DEVEL_KIT=v0.3.1 \
    LUA_VERSION=v0.10.15

RUN  apt-get update -y &&  apt-get upgrade -y &&\
    apt-get install build-essential wget libssl-dev libpcre3-dev gcc make zlib1g-dev -y && \
    wget http://nginx.org/download/nginx-$NGINX_VERSION.tar.gz  && tar zxf  nginx-$NGINX_VERSION.tar.gz  && \
    wget https://github.com/openresty/luajit2/archive/$LUAJIT_VERSION.tar.gz  && tar zxf $LUAJIT_VERSION.tar.gz   && \
    wget https://github.com/vision5/ngx_devel_kit/archive/$NGINX_DEVEL_KIT.tar.gz  && tar zxf $NGINX_DEVEL_KIT.tar.gz  && \
    wget https://github.com/openresty/lua-nginx-module/archive/$LUA_VERSION.tar.gz &&  tar zxf $LUA_VERSION.tar.gz  && \
    cd luajit2-2.1-20200102  && make install && \
    cd ../nginx-$NGINX_VERSION && \
    LUAJIT_LIB=/usr/local/lib LUAJIT_INC=/usr/local/include/luajit-2.1 \
    ./configure \
    --without-http_gzip_module \
    --add-module=/ngx_devel_kit-0.3.1 \
    --add-module=/lua-nginx-module-0.10.15 && \
    make install



FROM debian:9
WORKDIR /usr/local/nginx/sbin
ENV LD_LIBRARY_PATH /usr/local/lib
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/lib/* ../../lib/
RUN mkdir ../logs ../conf && chmod +x nginx
EXPOSE 80 443
CMD [ "./nginx", "-g",  "daemon off;" ]
 
